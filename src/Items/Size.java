package Items;

public class Size {

    // region Variables
    private final String name;
    private final float multiplier;
    // endregion

    // region Constructor
    public Size (String _name, float _multiplier) {
        this.name = _name;
        this.multiplier = _multiplier;
    }
    // endregion

    // region Getters
    public String getName() {
        return this.name;
    }

    public float getMultiplier() {
        return this.multiplier;
    }
    // endregion
}
