package Items;

public class Topping {

    // region Variables
    private final String name;
    private final float price;
    // endregion

    // region Constructor
    public Topping(String _name, float _price) {
        this.name = _name;
        this.price = _price;
    }
    // endregion

    // region Getters
    public String getName() {
        return this.name;
    }

    public float getPrice() {
        return this.price;
    }
    // endregion

}
