package Items;

public class Pizza {

    // region Variables
    private final String details;
    private final float price;
    // endregion

    // region Constructor
    public Pizza(String _details, float _price){
        this.details = _details;
        this.price = _price;
    }
    // endregion

    // region Getters
    public String getDetails() {
        return this.details;
    }

    public float getPrice() {
        return this.price;
    }
    // endregion

}
