package menuing;

import Items.Pizza;
import Items.Size;
import Items.Topping;

public class Menu {

    // region Variables
    // Since we can't add new items within the program, I just store the items in an array
    public final Pizza[] pizzas = {
            new Pizza("London: Tomato, cheese, onions, ham", 65f),
            new Pizza("Dublin: Tomato, cheese, onions, minced meat", 70f),
            new Pizza("Edinburgh: Tomato, cheese, garlic, minced meat", 60f),
            new Pizza("Paris: Tomato, cheese, ham", 75f),
            new Pizza("Bruxelles: Tomato, cheese, pepperoni", 70f),
            new Pizza("Berlin: Tomato, cheese, onions, garlic, minced meat", 65f),
            new Pizza("Copenhagen: Tomato, cheese, salad, minced meat", 70f),
            new Pizza("Amsterdam: Tomato, cheese, salad, garlic, ham", 75f),
            new Pizza("Madrid: Tomato, cheese, chili, ham", 65f),
            new Pizza("Rome: Tomato, cheese, chili, minced meat", 60f)
    };

    // Since we can't add new items within the program, I just store the items in an array
    public final Topping[] toppings = {
            new Topping("Nothing", 0f),
            new Topping("Extra onions", 2.5f),
            new Topping("Extra garlic", 2.5f),
            new Topping("Extra chili", 2.5f),
            new Topping("Extra mushrooms", 2.5f),
            new Topping("Extra tomatoes", 5f),
            new Topping("Extra cheese", 5f),
            new Topping("Extra salad", 5f),
            new Topping("Extra ham", 7.5f),
            new Topping("Extra minced meat", 7.5f),
            new Topping("Extra pepperoni", 7.5f)
    };

    // Since we can't add new items within the program, I just store the items in an array
    private final Size[] sizes = {
            new Size("Child", 0.75f),
            new Size("Standard", 1f),
            new Size("Family", 1.5f)
    };
    // endregion

    // region Presenting method
    public void present() {
        System.out.println("PIZZAS");
        for (int i = 0; i != pizzas.length; i++) {
            if (i < 9) {
                System.out.println("0" + (i+1) + " " + formatMenuItem(pizzas[i].getDetails(), pizzas[i].getPrice(), "Pizza"));
            } else {
                System.out.println((i+1) + " " + formatMenuItem(pizzas[i].getDetails(), pizzas[i].getPrice(), "Pizza"));
            }
        }
        System.out.println("================================================================================|");
        System.out.println("TOPPINGS");
        for (int i = 0; i != toppings.length; i++) {
            if (i < 9) {
                System.out.println("0" + (i+1) + " " + formatMenuItem(toppings[i].getName(), toppings[i].getPrice(), "Topping"));
            } else {
                System.out.println((i+1) + " " + formatMenuItem(toppings[i].getName(), toppings[i].getPrice(), "Topping"));
            }
        }
        System.out.println("=============================================|");
        System.out.println("SIZES");
        for (int i = 0; i != sizes.length; i++) {
            if (i < 9) {
                System.out.println("0" + (i+1) + " " + formatMenuItem(sizes[i].getName(), sizes[i].getMultiplier(), "Size"));
            } else {
                System.out.println((i+1) + " " + formatMenuItem(sizes[i].getName(), sizes[i].getMultiplier(), "Size"));
            }
        }
        System.out.println("=============================|");
    }
    // endregion

    // region Getters
    public Pizza getPizza(int id) {
        return this.pizzas[id];
    }

    public Topping getTopping(int id) {
        return this.toppings[id];
    }

    public Size getSize(int id) {
        return this.sizes[id];
    }
    // endregion

    // region Formatting methods
    // This method is used to present the menu item in a clean way
    public String formatMenuItem(String _name, float _price, String category) {

        StringBuilder returnVal = new StringBuilder(_name);

        // This value is only for formatting
        int maxLength = 0;

        switch(category) {
            case "Pizza":
                maxLength = 51;
                break;
            case "Topping":
                maxLength = 17;
                break;
            case "Size":
                maxLength = 8;
                break;
        }

        // region formatting with spaces
        while (returnVal.length() != maxLength) {
            returnVal.append(" ");
        }

        returnVal.append("          ");
        // endregion

        // To define whether it treats the item as size or pizza/topping
        if (category == "Size") {
            returnVal.append(Float.toString(_price * 100f)).append(" %");
        } else {
            returnVal.append("Price DKK: ").append(Float.toString(_price));
        }

        return returnVal.toString();

    }
    // endregion

}
