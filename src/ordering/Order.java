package ordering;

import java.util.ArrayList;

public class Order {

    // region Variables
    private ArrayList<OrderItem> items = new ArrayList<OrderItem>();
    private float sum;
    // endregion

    // region Getters
    public ArrayList<OrderItem> getItems() {
        return this.items;
    }

    public float getSum() {
        return this.sum;
    }

    public void getReceipt() {
        for(int i = 0; i != items.size(); i++) {
            System.out.println("PIZZA " + (i+1));
            System.out.println(items.get(i).getPizza().getDetails() + " = " + items.get(i).getPizza().getPrice() + " DKK");
            for (int x = 0; x != items.get(i).getToppings().size(); x++) {
                if (items.get(i).getToppings().get(x).getName() != "Nothing") {
                    System.out.println("+" + items.get(i).getToppings().get(x).getName() + " = " + items.get(i).getToppings().get(x).getPrice() + " DKK");
                }
            }
            System.out.println(items.get(i).getSize().getName() + " size = " + (items.get(i).getSize().getMultiplier()*100) + " %");
            System.out.println("TOTAL PRICE FOR PIZZA " + (i+1) + " = " + items.get(i).getPrice());
            System.out.println("================================================|");
        }
        setSum();
        System.out.println("TOTAL = " + this.getSum() + " DKK");
    }
    // endregion

    // region Setters
    public void setItems(ArrayList<OrderItem> _items) {
        this.items = _items;
    }

    public void setSum() {
        this.sum = 0f; // Prevent over-calculation

        // Loop through items and get the total price of all items
        for (int i = 0; i != this.items.size(); i++) {
            this.sum += this.items.get(i).getPrice();
        }

    }
    // endregion

    // region Item ArrayList manipulators
    public void addItem(OrderItem _item) {
        this.items.add(_item);
    }

    public void removeItem(int i) {
        this.items.remove(i);
    }
    // endregion
}
