package ordering;

import Items.Pizza;
import Items.Size;
import Items.Topping;
import menuing.Menu;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class OrderItem {

    // region Variables
    private Pizza pizza;
    private ArrayList<Topping> toppings = new ArrayList<Topping>();
    private Size size;
    private float price;
    // endregion

    // region Getters
    public Pizza getPizza() {
        return this.pizza;
    }

    public ArrayList<Topping> getToppings() {
        return this.toppings;
    }

    public Size getSize() {
        return this.size;
    }

    public float getPrice() {

        // To prevent over-calculation
        this.price = 0f;

        // Adding pizza price to the order price
        this.price = this.pizza.getPrice();

        // Adding topping prices to the order price
        for (int i = 0; i != this.toppings.size(); i++) {
            this.price += this.toppings.get(i).getPrice();
        }

        // Applying size multiplier to the order price
        this.price = this.price * size.getMultiplier();

        return this.price;
    }
    // endregion

    // region Setters
    public void setPizza(Pizza _pizza) {
        this.pizza = _pizza;
    }

    public void setToppings(ArrayList<Topping> _toppings) {
        this.toppings = _toppings;
    }

    public void setSize(Size _size) {
        this.size = _size;
    }
    // endregion

}
