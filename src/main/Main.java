/**
 * This program is for ordering pizza
 * @author Oliver Bundgaard
 * @since 6.10.2021
 * @version 0.0.1
 */

package main;

import Items.Pizza;
import Items.Size;
import Items.Topping;

import menuing.Menu;

import ordering.Order;
import ordering.OrderItem;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Scanner
        Scanner sc = new Scanner(System.in);

        // Creating and presenting the menu
        Menu menu = new Menu();
        menu.present();

        // Creating the order
        Order order = new Order();

        // Ordering
        int pizzaNum = 1;
        boolean wannaOrder = true;

        System.out.println("PIZZA " + pizzaNum);
        order.addItem(createOrder());

        System.out.println("===========================================|");

        // Receipt
        order.getReceipt();

    }

    static OrderItem createOrder() {
        // To get the pizzas, toppings and sizes
        Menu menu = new Menu();

        // Return value
        OrderItem item = new OrderItem();

        // First, we select the pizza
        Pizza selectedPizza = menu.getPizza(getID(10, 1, "Pizza").get(0) - 1);

        // Second, we select the toppings we want
        ArrayList<Integer> toppingIDs = getID(11, 10, "Topping");
        ArrayList<Topping> selectedTopping = new ArrayList<Topping>();

        for(int i = 0; i != toppingIDs.size() - 1; i++) {
            if (toppingIDs.get(i) != 0) {
                selectedTopping.add(i, menu.getTopping(toppingIDs.get(i) - 1));
            }
        }

        // Last, we select the size of the pizza
        Size selectedSize = menu.getSize(getID(3, 1, "Size").get(0) - 1);

        // Then we set the selected things to the order item
        item.setPizza(selectedPizza);
        item.setToppings(selectedTopping);
        item.setSize(selectedSize);
        item.getPrice();

        return item;
    }

    static ArrayList<Integer> getID(int max, int limit, String label) {

        /*
         * PARAMETERS:
         * max = largest valid id
         * limit = the amount of ids that can be returned
         * label = basically just for the user to see if it's pizza, topping or size they have to select
         */

        Scanner sc = new Scanner(System.in);

        // Will be storing the ID of the items
        ArrayList<Integer> returnVal = new ArrayList<Integer>();

        for (int i = 0; i != limit; i++) {
            returnVal.add(i, 0);

            System.out.print("Enter the id of the " + label + " you want (1-" + max + "): ");
            while(!sc.hasNextInt()) {
                sc.next();
                System.out.println("You have to enter a number!");
                System.out.print("Enter the id of the " + label + " you want (1-" + max + "): ");
            }
            returnVal.add(i, sc.nextInt());

            while (returnVal.get(i) < 1 || returnVal.get(i) > max) {
                System.out.println("You have to enter a number between 1 & " + max + "!");
                returnVal.add(i, 0);
                System.out.print("Enter the id of the " + label + " you want (1-" + max + "): ");
                returnVal.add(i, sc.nextInt());
            }

            // Return the current ids if the chosen topping is 1 / "None"
            if (label == "Topping" && returnVal.get(i) == 1) {
                return returnVal;
            }

        }

        return returnVal;

    }

}
